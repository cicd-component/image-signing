# Image Signing

## Usage

Use this component to enable container image signing in your project during `build` stage.
You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: gitlab.com/cicd-component/image-signing/image-signing@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

This will add a `container_signing` job to the pipeline.

In the build stage, you can build your docker image and push it. Further, you can extend that stage to sign your image like below

```yaml
# Build Stage
build-image:
  image: docker:24.0.2
  stage: build
  services:
    - docker:24.0.2-dind
  script:
    - docker build --tag $CI_REGISTRY_IMAGE:latest --file Dockerfile .
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker push $CI_REGISTRY_IMAGE:latest

# Container Signing Stage (Included from the component template)
container_signing:
  extends: build-image
  variables:
    COSIGN_ENABLED: "true"
```

The template should work without modifications but you can customize the template settings if
needed.

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `build`      | The stage where you want the job to be added |
| `cosign_enabled` |`true` | Enable the cosign for the `container_signing` job. Use `true` to enable it. |

For details, see the following links:
- https://docs.sigstore.dev/signing/quickstart/#getting-started-quick-start
- https://docs.sigstore.dev/signing/signing_with_containers/


Requirements:

- For auto-remediation, a readable Dockerfile in the root of the project or as defined by the
  CS_DOCKERFILE_PATH variable.

## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components
